{ nixpkgs ? <nixpkgs>
, system ? "x86_64-linux"
}:

with import "${nixpkgs}/lib";

let

  tests = import ./tests/all-tests.nix;
  makeTest = import "${nixpkgs}/nixos/tests/make-test.nix";

in

listToAttrs (map (t:
  let job = (makeTest (import t)) { inherit system; };
  in { name = removePrefix "vm-test-run-" job.name; value = job; }
) tests)
